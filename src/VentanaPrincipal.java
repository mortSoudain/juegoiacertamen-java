import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Scanner;
import javax.swing.JFrame;

public class VentanaPrincipal extends JFrame implements Constantes{
    
// Se declaran el Lienzo y el hilo que reproduce la música
    public Lienzo lienzo;
    public HiloMusica player;
    
//Constructor

    public VentanaPrincipal() {

        lienzo = new Lienzo();
        lienzo.setFocusable(true);
        lienzo.requestFocus();
        this.getContentPane().add(lienzo);
        this.setTitle("Rick & Morty - Letters Delivery");
        
        // Le sumo unos int para que ocupe el espacio que debe ocupar
        // Bug 01
        this.setSize(SIZE_WIDTH+16, SIZE_HEIGHT+42);
        
        // Bloque que asigna posicion central del JFrame
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        
        // Reproduccion de musica
        player=new HiloMusica("recursos/musica/musica.wav",2);
        player.run();
        
    }
}
