
import java.awt.Color;
import java.util.Random;

public interface Constantes {
    
    //Constantes del juego
    public final int NUMBER_LETTERS=10;
    public final int NUMERO_DE_PORTALES=10;    
    public final int NUMBER_CARS=10;
    public final int NUMERO_DE_PEATONES=10;
    public final int RANGO_CELDA_VISION=4;
    
    
    public final int ALFA = 127;
    public final Color COLOR_FONDO = new Color(117,117,117,ALFA);
    public final Color COLOR_CAMINO = new Color(255,0,0,100);
    public final Color COLOR_TRANSPARENTE = new Color(0,0,0,0);
    
    public final int SIZE_CELL = 32;
    public final int SIZE_CELL_M = 16;
    public final int NUMBER_CELL_WIDTH = 37;
    public final int NUMBER_CELL_HEIGHT = 23;
    public final int NUMBER_CELL_CUADRA=7;
    public final int NUMERO_DE_CUADRAS = 21;
    public final int SIZE_WIDTH = SIZE_CELL * NUMBER_CELL_WIDTH;
    public final int SIZE_HEIGHT = SIZE_CELL * NUMBER_CELL_HEIGHT;
    
    //Para manejar los tipos de celdas
    
    public final char CARTERO = 'O';
    public final char PARED = 'D';
    public final char CARRETERA = 'A';
    public final char PORTAL = 'L';
    public final char PORTAL_CERRADO = 'T';
    public final char PEATON = 'N';
    public final char AUTOMOVIL = 'I';
    public final char ACERA = 'E';
    public final char PASOV = 'P';
    public final char PASOH = 'H';
    public final char PARADA = 'R';
    public final char AUTOBUS = 'B';
    public final char CAMINO = 'K';
    public final char VACIA = 'V';
    
    // para manejar direcciones
    public final char NULA = 'N';
    public final char ARRIBA = 'U';
    public final char ABAJO = 'D';
    public final char IZQUIERDA = 'L';
    public final char DERECHA = 'R';
    
    default int numeroAleatorio(int minimo, int maximo) {
        Random random = new Random();
        int numero_aleatorio = random.nextInt((maximo - minimo) + 1) + minimo;
        return numero_aleatorio;
    }
    
    default char direccionAleatoria() {
        int random = numeroAleatorio(0,3);
        switch(random){
            case 0:
                return ARRIBA;            
            case 1:
                return DERECHA;            
            case 2:
                return ABAJO;            
            case 3:
                return IZQUIERDA;
        }
        return NULA;
    }
}
