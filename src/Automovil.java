import java.util.TimerTask;

public class Automovil extends TimerTask implements Constantes{
    
    public Laberinto laberinto;
    //Celda creada solamente para manejar con mayor facilidad los indices del laberinto
    public Celda automovil;

    
    public Automovil(Laberinto laberinto) {
        this.laberinto=laberinto;
        
        // Validacion para crear los automoviles solo en las carreteras
        // Los automoviles se generan en posiciones aleatorias
        boolean creado=false;
        do{
            int x = numeroAleatorio(0,NUMBER_CELL_WIDTH-1);
            int y = numeroAleatorio(0,NUMBER_CELL_HEIGHT-1);
            
            if(laberinto.celdas[x][y].tipo==CARRETERA){
                automovil=new Celda(x,y,AUTOMOVIL);
                creado=true;
            }
            
        } while(!creado);

        
        automovil.celdaAnterior = laberinto.celdas[automovil.x][automovil.y].tipo;
        laberinto.celdas[automovil.x][automovil.y].tipo=AUTOMOVIL;
        laberinto.celdas[automovil.x][automovil.y].direccion=direccionAleatoria();
    }
    
    public void moverAutomovil(){
        switch(laberinto.celdas[automovil.x][automovil.y].direccion){
            case IZQUIERDA:
                moverAutoIzquierda();
                break;
            case ARRIBA:
                moverAutoArriba();
                break;
            case DERECHA:
                moverAutoDerecha();
                break;
            case ABAJO:
                moverAutoAbajo();
                break;
        }
    }
    
    private void moverAutoArriba() {
        if (automovil.y > 0) { // Si el automovil no ha llegado al limite superior
            if(!hayObstaculo(ARRIBA)){ //Si no hay obstaculo, avanzar
                avanzar(ARRIBA);
            } else { //Si hay obstaculo, cambiar direccion
                laberinto.celdas[automovil.x][automovil.y].direccion=DERECHA;
            }
        } else { //Auto en el limite superior
            laberinto.celdas[automovil.x][automovil.y].tipo=automovil.celdaAnterior;
            //automovil.x=numeroAleatorio(0,NUMBER_CELL_WIDTH-1);
            automovil.y=NUMBER_CELL_HEIGHT-1;
            laberinto.celdas[automovil.x][automovil.y].tipo=AUTOMOVIL;
            laberinto.celdas[automovil.x][automovil.y].direccion=ARRIBA;
        }
    }
    
    private void moverAutoAbajo() {
        if (automovil.y < NUMBER_CELL_HEIGHT - 1) { //si se puede mover
            if(!hayObstaculo(ABAJO)){
                avanzar(ABAJO);
            } else {
                laberinto.celdas[automovil.x][automovil.y].direccion=IZQUIERDA;
            }
        } else{
            laberinto.celdas[automovil.x][automovil.y].tipo=automovil.celdaAnterior;
            //automovil.x=numeroAleatorio(0,NUMBER_CELL_WIDTH-1);
            automovil.y=0;
            laberinto.celdas[automovil.x][automovil.y].tipo=AUTOMOVIL;
            laberinto.celdas[automovil.x][automovil.y].direccion=ABAJO;
        }
    }
    
    private void moverAutoDerecha() {
        if (automovil.x < NUMBER_CELL_WIDTH - 1) { //si se puede mover
            if(!hayObstaculo(DERECHA)){
                avanzar(DERECHA);
            } else {
                laberinto.celdas[automovil.x][automovil.y].direccion=ABAJO;
            }
        } else {
            laberinto.celdas[automovil.x][automovil.y].tipo=automovil.celdaAnterior;
            automovil.x=0;
            //automovil.y=numeroAleatorio(0,NUMBER_CELL_HEIGHT-1);
            laberinto.celdas[automovil.x][automovil.y].tipo=AUTOMOVIL;
            laberinto.celdas[automovil.x][automovil.y].direccion=DERECHA;
        }
    }
    
    private void moverAutoIzquierda() {
        if (automovil.x > 0) { //si se puede mover
            if(!hayObstaculo(IZQUIERDA)){
                avanzar(IZQUIERDA);
            } else{
                laberinto.celdas[automovil.x][automovil.y].direccion=ARRIBA;
            }
        } else {
            laberinto.celdas[automovil.x][automovil.y].tipo=automovil.celdaAnterior;
            automovil.x=NUMBER_CELL_WIDTH-1;
            //automovil.y=numeroAleatorio(0,NUMBER_CELL_HEIGHT-1);
            laberinto.celdas[automovil.x][automovil.y].tipo=AUTOMOVIL;
            laberinto.celdas[automovil.x][automovil.y].direccion=IZQUIERDA;
        }
    }
    
    public boolean hayObstaculo(char dir){
        switch(dir){
            case ARRIBA:
                if(laberinto.celdas[automovil.x][automovil.y - 1].tipo != CARRETERA &&
                        laberinto.celdas[automovil.x][automovil.y-1].tipo != PASOH &&
                        laberinto.celdas[automovil.x][automovil.y-1].tipo != PASOV)
                    return true;
                break;
            case ABAJO:
                if(laberinto.celdas[automovil.x][automovil.y + 1].tipo != CARRETERA &&
                        laberinto.celdas[automovil.x][automovil.y+1].tipo != PASOH &&
                        laberinto.celdas[automovil.x][automovil.y+1].tipo != PASOV)
                    return true;
                
                break;
            case IZQUIERDA:
                if(laberinto.celdas[automovil.x-1][automovil.y].tipo != CARRETERA &&
                        laberinto.celdas[automovil.x-1][automovil.y].tipo != PASOH &&
                        laberinto.celdas[automovil.x-1][automovil.y].tipo != PASOV){
                    return true;
                }
                break;
            case DERECHA:
                if(laberinto.celdas[automovil.x+1][automovil.y].tipo != CARRETERA &&
                        laberinto.celdas[automovil.x+1][automovil.y].tipo != PASOH &&
                        laberinto.celdas[automovil.x+1][automovil.y].tipo != PASOV)
                    return true;
                break;
        }
        return false;
    }
    
    public void avanzar(char dir){
        switch(dir){
            case ARRIBA:
                laberinto.celdas[automovil.x][automovil.y].tipo = automovil.celdaAnterior;
                laberinto.celdas[automovil.x][automovil.y].direccion = NULA;
                automovil.celdaAnterior = laberinto.celdas[automovil.x][automovil.y-1].tipo;
                automovil.y = automovil.y - 1;
                laberinto.celdas[automovil.x][automovil.y].celdaAnterior=automovil.celdaAnterior;
                laberinto.celdas[automovil.x][automovil.y].color=laberinto.celdas[automovil.x][automovil.y+1].color;
                laberinto.celdas[automovil.x][automovil.y].tipo = AUTOMOVIL;
                laberinto.celdas[automovil.x][automovil.y].direccion = ARRIBA;
                laberinto.celdas[automovil.x][automovil.y].indexSprite=0;
                
                break;
            case 'D':
                laberinto.celdas[automovil.x][automovil.y].tipo = automovil.celdaAnterior;
                laberinto.celdas[automovil.x][automovil.y].direccion = NULA;
                automovil.celdaAnterior = laberinto.celdas[automovil.x][automovil.y+1].tipo;
                automovil.y = automovil.y + 1;
                laberinto.celdas[automovil.x][automovil.y].celdaAnterior=automovil.celdaAnterior;
                laberinto.celdas[automovil.x][automovil.y].color=laberinto.celdas[automovil.x][automovil.y-1].color;
                laberinto.celdas[automovil.x][automovil.y].tipo = AUTOMOVIL;
                laberinto.celdas[automovil.x][automovil.y].direccion = ABAJO;
                laberinto.celdas[automovil.x][automovil.y].indexSprite=2;
                
                break;
            case 'L':
                laberinto.celdas[automovil.x][automovil.y].tipo = automovil.celdaAnterior;
                laberinto.celdas[automovil.x][automovil.y].direccion = NULA;
                automovil.celdaAnterior = laberinto.celdas[automovil.x-1][automovil.y].tipo;
                automovil.x = automovil.x - 1;
                laberinto.celdas[automovil.x][automovil.y].celdaAnterior=automovil.celdaAnterior;
                laberinto.celdas[automovil.x][automovil.y].color=laberinto.celdas[automovil.x+1][automovil.y].color;
                laberinto.celdas[automovil.x][automovil.y].tipo = AUTOMOVIL;
                laberinto.celdas[automovil.x][automovil.y].direccion = IZQUIERDA;
                laberinto.celdas[automovil.x][automovil.y].indexSprite=3;
                
                break;
            case 'R':
                laberinto.celdas[automovil.x][automovil.y].tipo = automovil.celdaAnterior;
                laberinto.celdas[automovil.x][automovil.y].direccion = NULA;
                automovil.celdaAnterior = laberinto.celdas[automovil.x+1][automovil.y].tipo;
                automovil.x = automovil.x + 1;    
                laberinto.celdas[automovil.x][automovil.y].celdaAnterior=automovil.celdaAnterior;
                laberinto.celdas[automovil.x][automovil.y].color=laberinto.celdas[automovil.x-1][automovil.y].color;
                laberinto.celdas[automovil.x][automovil.y].tipo = AUTOMOVIL;
                laberinto.celdas[automovil.x][automovil.y].direccion = DERECHA;
                laberinto.celdas[automovil.x][automovil.y].indexSprite=1;
                
                break;
        }
    }
    
    @Override
    public void run() {
        moverAutomovil();
        laberinto.lienzoPadre.repaint();
    }
    
}