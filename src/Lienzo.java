
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.util.Timer;

public class Lienzo extends Canvas implements Constantes {
    
//para pintar el lienzo
    public Laberinto laberinto;
    public Image fondo;

    //para implementar el doble buffer
    public Graphics graficoBuffer;
    public Image imagenBuffer;

    public Lienzo() {
        
        
        try {
            fondo = ImageIO.read(new File("recursos/imagenes/fondo.jpg"));
        } catch (IOException e) {
            System.out.println(e.toString());
        }
        
        laberinto=new Laberinto(this);

        //escuchador eventos de teclado
        addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent evt) {
                laberinto.jugador.moverCelda(evt);
                repaint();
            }
        });
        
        //añadimos el escuchador
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                System.out.print("Click en X : " + evt.getX());
                System.out.println(" Click en Y : " + evt.getY());
                
                // recorro laberinto buscando celda seleccionada
                if(evt.getButton() == MouseEvent.BUTTON3){
                for(int i=0; i < NUMBER_CELL_WIDTH; i++){
                    for ( int j=0 ; j < NUMBER_CELL_HEIGHT ; j++){
                        // encontramos la celda seleccionada
                        if(laberinto.celdas[i][j].esCeldaSeleccionada(evt.getX(),evt.getY())){
                            
                            // La celda se hace tipo PORTAL y se añade a los destinos de la busqueda
                            System.out.print("Celda Seleccionada : " + laberinto.celdas[i][j]);
                            laberinto.jugador.busquedaPorAnchura.destinos.add(new Estado(i,j,'N',null));
                            laberinto.jugador.busquedaPorAnchura.pararDeBuscar=false;
                            laberinto.jugador.busquedaPorAnchura.pararDeMoverse=false;
                            laberinto.celdas[i][j].tipo=PORTAL;
                        }
                    }
                }
                }
                repaint();
            }
        });
    }
    
    @Override
    public void update(Graphics g) {
        
        //inicialización del buffer gráfico mediante la imagen
        if(graficoBuffer==null){
            imagenBuffer=createImage(this.getWidth(),this.getHeight());
            graficoBuffer=imagenBuffer.getGraphics();
        }
        
        //volcamos color de fondo e imagen en el nuevo buffer grafico
        graficoBuffer.setColor(getBackground());
        graficoBuffer.fillRect(0,0,this.getWidth(),this.getHeight());
        graficoBuffer.drawImage(fondo, 0, 0, null);
        laberinto.update(graficoBuffer);
        //pintamos la imagen previa
        g.drawImage(imagenBuffer, 0, 0, null);
        
    }

    @Override
    public void paint(Graphics g) {
        update(g);
    }
    
}
