import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JComponent;

public class Carta extends JComponent implements Constantes {
    
    public int x;
    public int y;
    public BufferedImage carta;
    
    public Estado destino;
    
    
    //public BufferedImage carta;
    
//constructor
    public Carta(int x, int y) {
        this.x = x;
        this.y = y;       
        try {
            carta = ImageIO.read(new File("recursos/imagenes/carta.png"));
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
    
    public void añadirDestino(Estado destino){
        this.destino=destino;
    }
    
//metodo para dibujar carta, hace uso de drawRect
    @Override
    public void update(Graphics g) {
        g.drawImage(carta, x, y, null);
    }
    
    @Override
    public void paintComponent(Graphics g) {
        update(g);
    }
}
