
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.TimerTask;

public class BusquedaPorAnchura extends TimerTask implements Constantes{
    
    public Laberinto laberinto;
    public PriorityQueue<Estado> colaEstados;
    public ArrayList<Estado> historial;
    public ArrayList<Character> pasos;
    public int index_pasos;
    public Estado inicial;
    public Estado objetivo;
    public Estado temp;
    public boolean exito;
    
    //para tener un busqueda anchura multiobjetivo
    public Cartero jugador;
    public ArrayList<Estado> destinos;
    public boolean pararDeBuscar;
    public boolean pararDeMoverse;
    
        
    public BusquedaPorAnchura (Laberinto laberinto, Cartero jugador) {
        
        this.laberinto=laberinto;
        colaEstados=new PriorityQueue();
        historial=new ArrayList<>();
        pasos=new ArrayList<>();
        index_pasos=0;
        exito=false;
        
        this.jugador=jugador;
        destinos=new ArrayList<>();
        pararDeBuscar=false;
        pararDeMoverse=false;
        
    }
    
    public boolean buscar(Estado inicial,Estado objetivo) {
        
        System.out.println("Estado inicial"+inicial.toString());
        System.out.println("Estado objetivo"+objetivo.toString());
        index_pasos=0;
        this.objetivo=objetivo;
        exito=false;
        
        // añadimos los estados a la cola de estados y al historial
        colaEstados.add(inicial);
        historial.add(inicial);
        
        //si el inicial es final, salimos
        if ( inicial.equals(objetivo)) exito=true;
        
        // si no mientras que la cola no este vacia y no hayamos
        // alcanzado el meta hacemos lo siguiente
        while ( !colaEstados.isEmpty() && !exito ){

            //tomamos el primero y lo quitamos de cola de estad
            temp=colaEstados.poll();
            
            //lo exploramos, es decir, generamos sus sucesores,
            // es decir, los estados a los que podemos ir desde el
            // estado actual
            
            //System.out.println("Estado Temporal:"+temp);
            
            moverArriba(temp);
            moverAbajo(temp);
            moverIzquierda(temp);
            moverDerecha(temp);
        }
        if ( exito ) {
            System.out.println("Ruta calculada");
            this.calcularRuta();
            return true;
        }
        else {
            System.out.println("La ruta no pudo calcularse");
            return false;
        }
    }


        //distancia adversario
    public double distancia(int x1,int y1, int x2, int y2) {
        double valor;
        double parte1=Math.pow(Math.abs(x1-x2),2);
        double parte2=Math.pow(Math.abs(y1-y2),2);
        parte1+=parte2;
        valor=Math.sqrt(parte1);
        return valor;
    }
    
    
    /*
    Por último implementamos los métodos de expansión que consiste en generar
            los estados sucesores:
    */
    
    private void moverArriba(Estado e) {
        if (jugador.cartero.y > 0) { //si se puede mover
            if(!(laberinto.celdas[e.x][e.y - 1].tipo == PARED
                        || laberinto.celdas[e.x][e.y - 1].tipo == PEATON
                        || laberinto.celdas[e.x][e.y - 1].tipo == AUTOMOVIL
                        || laberinto.celdas[e.x][e.y - 1].tipo == CARRETERA
                        || laberinto.celdas[e.x][e.y - 1].tipo == PORTAL_CERRADO
                        || laberinto.celdas[e.x][e.y - 1].tipo == VACIA)){
            Estado arriba=new Estado(e.x,e.y-1,ARRIBA,e);
            //arriba.prioridad=distancia(arriba.x,arriba.y,objetivo.x,objetivo.y);
            arriba.prioridad=laberinto.celdas[arriba.x][arriba.y].cantidadPeatones+distancia(arriba.x,arriba.y,objetivo.x,objetivo.y);
            if ( !historial.contains(arriba)) {
                colaEstados.add(arriba);
                historial.add(arriba);
                if ( arriba.equals(objetivo)) {
                    objetivo=arriba;
                    exito=true;
                }
            }
        }
        }
    }//fin del metodo moverArriba
    private void moverAbajo(Estado e) {
        if (jugador.cartero.y < NUMBER_CELL_HEIGHT - 1) { //si se puede mover
            if(!(laberinto.celdas[e.x][e.y + 1].tipo == PARED
                        || laberinto.celdas[e.x][e.y + 1].tipo == PEATON
                        || laberinto.celdas[e.x][e.y + 1].tipo == AUTOMOVIL
                        || laberinto.celdas[e.x][e.y + 1].tipo == CARRETERA
                        || laberinto.celdas[e.x][e.y + 1].tipo == PORTAL_CERRADO
                        || laberinto.celdas[e.x][e.y + 1].tipo == VACIA)){
            Estado abajo=new Estado(e.x,e.y+1,ABAJO,e);
            //abajo.prioridad=distancia(abajo.x,abajo.y,objetivo.x,objetivo.y);
            abajo.prioridad=laberinto.celdas[abajo.x][abajo.y].cantidadPeatones+distancia(abajo.x,abajo.y,objetivo.x,objetivo.y);
            if ( !historial.contains(abajo)) {
                colaEstados.add(abajo);
                historial.add(abajo);
                if ( abajo.equals(objetivo)) {
                    objetivo=abajo;
                    exito=true;
                }
            }
        }
        }
    }
    private void moverIzquierda(Estado e) {
        if (jugador.cartero.x > 0) { //si se puede mover
            if(!(laberinto.celdas[e.x - 1][e.y].tipo == PARED
                        || laberinto.celdas[e.x - 1][e.y].tipo == PEATON
                        || laberinto.celdas[e.x - 1][e.y].tipo == AUTOMOVIL
                        || laberinto.celdas[e.x - 1][e.y].tipo == CARRETERA
                        || laberinto.celdas[e.x - 1][e.y].tipo == PORTAL_CERRADO
                        || laberinto.celdas[e.x - 1][e.y].tipo == VACIA)){
            Estado izquierda=new Estado(e.x-1,e.y,IZQUIERDA,e);
            //izquierda.prioridad=distancia(izquierda.x,izquierda.y,objetivo.x,objetivo.y);
            izquierda.prioridad=laberinto.celdas[izquierda.x][izquierda.y].cantidadPeatones+distancia(izquierda.x,izquierda.y,objetivo.x,objetivo.y);
            if ( !historial.contains(izquierda)) {
                colaEstados.add(izquierda);
                historial.add(izquierda);
                if ( izquierda.equals(objetivo)) {
                    objetivo=izquierda;
                    exito=true;
                }
            }
        }
        }
    }// fin del metodo izquierda
    private void moverDerecha(Estado e) {
        if (jugador.cartero.x < NUMBER_CELL_WIDTH - 1) { //si se puede mover
            if(!(laberinto.celdas[e.x + 1][e.y].tipo == PARED
                        || laberinto.celdas[e.x + 1][e.y].tipo == PEATON
                        || laberinto.celdas[e.x + 1][e.y].tipo == AUTOMOVIL
                        || laberinto.celdas[e.x + 1][e.y].tipo == CARRETERA
                        || laberinto.celdas[e.x + 1][e.y].tipo == PORTAL_CERRADO
                        || laberinto.celdas[e.x + 1][e.y].tipo == VACIA)){
            Estado derecha=new Estado(e.x+1,e.y,DERECHA,e);
            //derecha.prioridad=distancia(derecha.x,derecha.y,objetivo.x,objetivo.y);
            derecha.prioridad=laberinto.celdas[derecha.x][derecha.y].cantidadPeatones+distancia(derecha.x,derecha.y,objetivo.x,objetivo.y);
            if ( !historial.contains(derecha)){
                colaEstados.add(derecha);
                historial.add(derecha);
                if ( derecha.equals(objetivo)) {
                    objetivo=derecha;
                    exito=true;
                }
            }
            }
        }
    }
    /*
    Una vez construido el corazón del algoritmo de búsqueda, si hubo exito
    entonces podemos reconstruir la soluciòn:
    */
    
    public void calcularRuta() {
        
        Estado predecesor=objetivo;
        // Se recorre todo el laberinto y se borran los caminos marcados
        for (int i = 0; i < NUMBER_CELL_WIDTH; i++) {
            for (int j = 0; j < NUMBER_CELL_HEIGHT; j++) {
                laberinto.celdas[i][j].caminoActivado=false;
            }
        }
        
        // se recorre el camino de la solucion, de atrás hacia adelante
        do{
            // Se pinta el camino en el laberinto
            if(predecesor.predecesor!=null){
            laberinto.celdas[predecesor.x][predecesor.y].caminoActivado=true;
            }
            pasos.add(0,predecesor.oper);
            predecesor=predecesor.predecesor;
        }while ( predecesor != null);
        index_pasos=pasos.size()-1;
    }
    /*
    En pasos están almacenados las acciones a realizar, por tanto el método
    run() tendrá la siguiente implementación:
    */
    
    @Override
    public void run() {
        //solo cuando quedan destinos donde ir
        if (!pararDeBuscar ) {
            //inicializacion de la busqueda
            colaEstados.clear();
            historial.clear();
            pasos.clear();
            Estado subinicial,subobjetivo;
            boolean resultado;
            do{
                //el estado inicial es donde estoy
                subinicial=new Estado(jugador.cartero.x,
                        jugador.cartero.y,'N',null);
                //el estado final es a donde quiero ir
                subobjetivo=destinos.get(0);
                //busco ruta
                resultado=this.buscar(subinicial,subobjetivo);
                if(!resultado){
                    pararDeMoverse=true;
                    resultado=true;
                } else {
                    pararDeMoverse=false;
                }
                
                if ( subinicial.equals(subobjetivo))
                    destinos.remove(subobjetivo);
                if ( destinos.isEmpty() ) {
                    System.out.println("Se acabo a donde ir");
                    
                    // el hilo no se cierra, se maneja mejor con parar...
                    pararDeBuscar=true;
                    //this.cancel();
                }
            } while (!resultado && !destinos.isEmpty());
            
        }
            
        if(!pararDeMoverse){
            if ( pasos.size() > 1 ) {
                laberinto.celdas[jugador.cartero.x][jugador.cartero.y].caminoActivado=false;
                switch(pasos.get(1)) {
                    case ABAJO: jugador.moverCeldaAbajo();break;
                    case ARRIBA: jugador.moverCeldaArriba();break;
                    case DERECHA: jugador.moverCeldaDerecha();break;
                    case IZQUIERDA: jugador.moverCeldaIzquierda();break;
                }
                laberinto.lienzoPadre.repaint();
            }
        }
            
        
    }
    
}
