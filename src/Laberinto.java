import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Timer;
import javax.swing.JComponent;

public class Laberinto extends JComponent implements Constantes {
    
    public Celda[][] celdas;
    public ArrayList<Cuadra> cuadras;
    
    public Lienzo lienzoPadre;
    
    // Para lanzar las entidades moviles
    public Timer lanzadorTareas;
    
    // Se declaran entidades moviles
    public Cartero jugador;
    public ArrayList<Automovil> autos;
    public ArrayList<Peaton> peatones;
    public ArrayList<Celda> portales;
    
    int cantidadCartas;
    int cantidadPortales;

    
    public Laberinto(Lienzo lienzoPadre) {
        
        // Se inicializa el arreglo que contendrá las celdas que componen el laberinto
        celdas = new Celda[NUMBER_CELL_WIDTH][NUMBER_CELL_HEIGHT];
        cuadras=new ArrayList();
        autos = new ArrayList();
        peatones = new ArrayList();
        portales = new ArrayList();
        
        // Antes de inicializar cualquier variable se piden la cantidad de portales y cartas a ser creados
                
        System.out.println("Ingrese N (Cantidad de cartas):");
        Scanner entradaEscaner = new Scanner (System.in); //Creación de un objeto Scanner
        cantidadCartas = entradaEscaner.nextInt(); //Invocamos un método sobre un objeto Scanner
        System.out.println ("La cantidad de cartas es : \"" + cantidadCartas +"\"");
        
        System.out.println("Ingrese N (Cantidad de portales):");
        cantidadPortales = entradaEscaner.nextInt(); //Invocamos un método sobre un objeto Scanner
        System.out.println ("La cantidad de portales es : \"" + cantidadPortales +"\"");
        
        for (int i=0; i<cantidadCartas;i++){
            
        }

        // Funcion para leer archivos txt y traspasar la informacion a las celdas
        //leerMapaDesdeTxt(MAPA);
        generarMapa(cantidadPortales);
        
        // Obtiene la info gráfica del lienzo en el estado anterior y la guarda para re pintarla
        this.lienzoPadre=lienzoPadre;
        
        
        // Se inicializan las entidades moviles
        
        
        // Se inicializan automoviles
        jugador=new Cartero(this);
        for(int i=0; i<NUMBER_CARS;i++){
            autos.add(new Automovil(this));
        }
        //Se inicializan peatones
        for(int i=0; i<NUMERO_DE_PEATONES;i++){
            peatones.add(new Peaton(this));
        }
        

        
        
        // Se lanzan los automatas(?) de las entidades moviles
        lanzadorTareas=new Timer();
        lanzadorTareas.scheduleAtFixedRate(jugador.busquedaPorAnchura,0,1000);
   
        autos.forEach((auto) -> {
            lanzadorTareas.scheduleAtFixedRate(auto,0,numeroAleatorio(250,500));
        }); 
        peatones.forEach((peaton) -> {
            lanzadorTareas.scheduleAtFixedRate(peaton,0,numeroAleatorio(250,500));
        });

        
    }
    
    // Funcion que lee la información del archivo de texto y la pasa a las celdas
    public void leerMapaDesdeTxt(String mapa){
        
        File archivo = null;
        FileReader fileread = null;
        BufferedReader bufferread = null;
        int contador =0;
        
        try {
            // Apertura del fichero y creacion de BufferedReader para
            // leer linea de archivo(disponer del metodo readLine()).
            archivo = new File ("recursos/mapas/"+mapa+".txt");
            fileread = new FileReader (archivo);
            bufferread = new BufferedReader(fileread);
            
            // Lectura del fichero
            String linea;
            while((linea=bufferread.readLine()) != null){
                for(int i=0;i<NUMBER_CELL_WIDTH;i++){
                    celdas[i][contador] = new Celda(i * SIZE_CELL, contador * SIZE_CELL, linea.charAt(i));
                }
                contador++;
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }finally{
            // En el finally cerramos el fichero
            try{
                if( null != fileread ){
                    fileread.close();
                }
            }catch (Exception e2){
                e2.printStackTrace();
            }
        }
        
    }
    
    // Funciones Gráficas
    @Override
    public void paintComponent(Graphics g) {
        update(g);
    }
    
    @Override
    public void update(Graphics g) {
        
        // Se pinta el laberinto
        int contadorPortales = 0;
        for (int i = 0; i < NUMBER_CELL_WIDTH; i++) {
            for (int j = 0; j < NUMBER_CELL_HEIGHT; j++) {
                celdas[i][j].update(g);
                // Si la celda es vacia, imprime la cantidad de peatones (fx cruadras)
                if(celdas[i][j].tipo==VACIA){
                    int fontSize = 20;
                    g.setFont(new Font("TimesRoman", Font.BOLD, fontSize));
                    g.setColor(Color.white);
                    g.drawString(""+celdas[i][j].cantidadPeatones, (i*SIZE_CELL)+6, (j*SIZE_CELL)+21);
                } /*else {
                    if(celdas[i][j].tipo==PORTAL){
                        contadorPortales++;
                        int fontSize = 20;
                        g.setFont(new Font("TimesRoman", Font.BOLD, fontSize));
                        g.setColor(Color.white);
                        g.drawString(""+contadorPortales, (i*SIZE_CELL)+6, (j*SIZE_CELL)+21);
                    }
                }*/

            }
        }
        
        // Se recorren los portales y se imprime su numero
        for(int i=0;i<portales.size();i++){
            System.out.println(portales.get(i).x);
            System.out.println(portales.get(i).y);
            int fontSize = 20;
            g.setFont(new Font("TimesRoman", Font.BOLD, fontSize));
            g.setColor(Color.red);
            g.drawString(""+i, (portales.get(i).x)+6, (portales.get(i).y)+21);
        }
        

        
        // Si el jugador tiene cartas
        if(jugador.cantidadCartasActual>0){
            
            // Se pintan las cartas
            for (int i = 0; i < jugador.cantidadCartasActual; i++) {
                jugador.cartas[i].update(g);
            }
            
            // Si el cartero esta en el portal, se pinta mensaje
            /*
            if(jugador.enPortal){
                System.out.println("El cartero llegó al portal");
                int fontSize = 50;
                g.setFont(new Font("TimesRoman", Font.PLAIN, fontSize));
                g.setColor(Color.blue);
                g.drawString("El cartero está en el Portal", 50, 50);
            
            }   */
        } 
        /*
        else { // Si no hay cartas, se pinta mensaje
            
            System.out.println("No hay más cartas!");
            int fontSize = 50;
            g.setFont(new Font("TimesRoman", Font.PLAIN, fontSize));
            g.setColor(Color.red);
            g.drawString("No hay más cartas!", 50, 50);
        
        }*/
        
        // Si el autobus tiene peatones
        /*
        if(autobus.cantidadPeatonesActual>0){
            // Se pintan los peatones
            for (int i = 0; i < autobus.cantidadPeatonesActual; i++) {
        autobus.peatones[i].update(g);
        }
        }
        */
        
    }
    
    public void generarMapa(int cantidadPortales){
        
        //Se genera un mapa vacío
        
        for (int i = 0; i < NUMBER_CELL_WIDTH; i++) {
            for (int j = 0; j < NUMBER_CELL_HEIGHT; j++) {
                if(i==0 || i==NUMBER_CELL_WIDTH-1 || j==0 || j==NUMBER_CELL_HEIGHT-1){
                    celdas[i][j]=new Celda(i * SIZE_CELL, j * SIZE_CELL, PARED);
                } else {
                    celdas[i][j]=new Celda(i * SIZE_CELL, j * SIZE_CELL, CARRETERA);
                }
            }
        }
        
        //Se añaden las cuadras al arreglo de cuadras, segun la posicion indicada
        
        for (int i = 0; i < NUMBER_CELL_WIDTH; i++) {
            for (int j = 0; j < NUMBER_CELL_HEIGHT; j++) {
                
                // Si en la posicion va una cuadra
                
                if(i%NUMBER_CELL_CUADRA==0 && j%NUMBER_CELL_CUADRA==0){
                    
                    //Crea una cuadra en la posicion y la añade al arreglo de cuadras
                    if(i+NUMBER_CELL_CUADRA<NUMBER_CELL_WIDTH && j+NUMBER_CELL_CUADRA<NUMBER_CELL_HEIGHT){
                        Cuadra cuadra = new Cuadra(i+1,j+1);
                        cuadras.add(cuadra);                    
                    }
                }
            }
        }
        
        // Inserta portales en cuadras aleatorias
        
        for (int i=0;i<cantidadPortales;i++){
            // Toma una cuadra aleatoria del arreglo de cuadras
            Cuadra cuadraSeleccionada = cuadras.get(numeroAleatorio(0,cuadras.size()-1)) ;
            //Inserta portal en esa cuadra
            cuadraSeleccionada.insertarPortalEnCuadra();    
        }
        
        // Se insertan las cuadras en el laberinto
        
        for(Cuadra cuadra:cuadras){
            cuadra.insertarCuadraEnLaberinto(this);
        }
        
        // Se guarda la posicion de los portales generados, en un arreglo de portales
        for (int i = 0; i < NUMBER_CELL_WIDTH; i++) {
            for (int j = 0; j < NUMBER_CELL_HEIGHT; j++) {
                if(this.celdas[i][j].tipo==PORTAL){
                    portales.add(this.celdas[i][j]);
                }
            }
        }
        
        
    }
}



