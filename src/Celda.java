
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import javax.swing.JComponent;

public class Celda extends JComponent implements Constantes {
    
    public int x;
    public int y;
    public char direccion;
    //carácter que almacena el tipo de celda que se encoentraba antes de llegar a ella
    public char celdaAnterior;
    
//nuevos atributos para manejar imagenes
    public char tipo;
    public BufferedImage cartero, pared, carretera, peaton, acera, pasov, pasoh, parada, portalCerrado;
    
//para manejar sprites
    public int color;
    public int indexSprite;
    public BufferedImage autos[][],portales[],autobuses[],casas[],imagenSprite;
    
    public int cantidadPeatones;
    public boolean caminoActivado;
    
    public Rectangle rectanguloCelda;
    public int numeroPortal;

    
    
//constructor
    public Celda(int x, int y, char tipo) {
        //Inicializando variables
        this.x = x;
        this.y = y;
        this.direccion=NULA;
        this.celdaAnterior=VACIA;
        
        this.tipo = tipo;
        this.color = numeroAleatorio(0,3);
        this.indexSprite=0;
        
        this.cantidadPeatones=0;
        this.numeroPortal=0;
        
        // para marcar camino de busquedas
        this.caminoActivado =false;
        // para verificar si click esta dentro de alguna celda
        this.rectanguloCelda=new Rectangle(x,y,SIZE_CELL,SIZE_CELL);
        
        try {
            cartero = ImageIO.read(new File("recursos/imagenes/cartero.png"));
            carretera = ImageIO.read(new File("recursos/imagenes/carretera.png"));
            peaton = ImageIO.read(new File("recursos/imagenes/peaton.png"));
            acera = ImageIO.read(new File("recursos/imagenes/acera.png"));
            pasov = ImageIO.read(new File("recursos/imagenes/pasov.png"));
            pasoh = ImageIO.read(new File("recursos/imagenes/pasoh.png"));
            parada  = ImageIO.read(new File("recursos/imagenes/parada.png"));
            portalCerrado  = ImageIO.read(new File("recursos/imagenes/portalCerrado.png"));
            
            //gestion de sprites
            //cargo la imagen de grupo de imagenes
            imagenSprite = ImageIO.read(new File("recursos/imagenes/autosprite.png"));
            //creo una array de 4 x 3
            autos = new BufferedImage[4][4];
            //lo recorro separando las imagenes
            for(int i = 0; i < 4; i++) {
                for(int j = 0; j < 4; j++) {
                    autos[i][j] = imagenSprite.getSubimage(i * SIZE_CELL, j * SIZE_CELL, SIZE_CELL, SIZE_CELL);
                }
            }
            imagenSprite = ImageIO.read(new File("recursos/imagenes/portalsprite.png"));
            //creo una array de 4 x 3
            portales = new BufferedImage[4];
            //lo recorro separando las imagenes
            for(int i = 0; i < 4; i++) {
                portales[i] = imagenSprite.getSubimage(i * SIZE_CELL,0, SIZE_CELL, SIZE_CELL);
            }
            
            imagenSprite = ImageIO.read(new File("recursos/imagenes/autobus.png"));
            //creo una array de 4 x 3
            autobuses = new BufferedImage[4];
            //lo recorro separando las imagenes
            for(int i = 0; i < 4; i++) {
                autobuses[i] = imagenSprite.getSubimage(i * SIZE_CELL, 0, SIZE_CELL, SIZE_CELL);
            }
            
            imagenSprite = ImageIO.read(new File("recursos/imagenes/casasprite.png"));
            //creo una array de 4 x 3
            casas = new BufferedImage[4];
            //lo recorro separando las imagenes
            for(int i = 0; i < 4; i++) {
                casas[i] = imagenSprite.getSubimage(0, i * SIZE_CELL, SIZE_CELL, SIZE_CELL);
            }
            
            
            
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
//metodo para dibujar celda, hace uso de drawRect
    @Override
    public void update(Graphics g) {
        dibujarCelda(g,celdaAnterior);
        dibujarCelda(g,tipo);
        dibujarCelda(g,CAMINO);
    }
    
    @Override
    public void paintComponent(Graphics g) {
        update(g);
    }
    
    public void dibujarCelda(Graphics g, char celda){
        switch (celda) {
            case CARTERO:
                g.drawImage(cartero, x, y, null);
                break;
            case PARED:
                g.drawImage(casas[color], x, y, null);
                break;
            case CARRETERA:
                g.drawImage(carretera, x, y, null);
                break;
            case PORTAL:
                switch(indexSprite){
                    case 0:
                        g.drawImage(portales[0], x, y, null);
                        indexSprite=3;
                        break;
                    case 1:
                        g.drawImage(portales[1], x, y, null);
                        indexSprite=0;
                        break;
                    case 2:
                        g.drawImage(portales[2], x, y, null);
                        indexSprite=1;
                        break;
                    case 3:
                        g.drawImage(portales[3], x, y, null);
                        indexSprite=2;
                        break;
                }
                break;
            case PORTAL_CERRADO:
                g.drawImage(portalCerrado, x, y, null);
                break;
            case PEATON:
                g.drawImage(peaton, x, y, null);
                break;
            case AUTOMOVIL:
                switch(direccion){
                    case ARRIBA:
                        g.drawImage(autos[0][color], x, y, null);
                        break;
                    case DERECHA:
                        g.drawImage(autos[1][color], x, y, null);
                        break;
                    case ABAJO:
                        g.drawImage(autos[2][color], x, y, null);
                        break;
                    case IZQUIERDA:
                        g.drawImage(autos[3][color], x, y, null);
                        break;
                }
                break;
            case PASOV:
                g.drawImage(pasov, x, y, null);
                break;
            case PASOH:
                g.drawImage(pasoh, x, y, null);
                break;
            case ACERA:
                g.drawImage(acera, x, y, null);
                break;
            case PARADA:
                g.drawImage(parada, x, y, null);
                break;
            case AUTOBUS:
                switch(direccion){
                    case ARRIBA:
                        g.drawImage(autobuses[0], x, y, null);
                        break;
                    case DERECHA:
                        g.drawImage(autobuses[1], x, y, null);
                        break;
                    case ABAJO:
                        g.drawImage(autobuses[2], x, y, null);
                        break;
                    case IZQUIERDA:
                        g.drawImage(autobuses[3], x, y, null);
                        break;
                }
                break;
            case CAMINO:
                if(caminoActivado){
                    g.setColor(COLOR_CAMINO);
                } else {
                     g.setColor(COLOR_TRANSPARENTE);
                }
                g.fillRect(x, y,SIZE_CELL,SIZE_CELL);
                break;            
            case VACIA:
                g.setColor(COLOR_FONDO);
                g.fillRect(x, y,SIZE_CELL,SIZE_CELL);
                break;
        }
    }
    
    //funcion para saber si un click se hace sobre una celda
    public boolean esCeldaSeleccionada(int xp,int yp) {
        if ( rectanguloCelda.contains(new Point(xp,yp)) ) {
            return true;
        } else return false;
    }

}