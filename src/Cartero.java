import java.awt.event.KeyEvent;
import java.util.Scanner;
import java.util.TimerTask;
import static javax.swing.JOptionPane.showMessageDialog;

public class Cartero extends TimerTask implements Constantes{

    public Laberinto laberinto;
    public Celda cartero;
    public Carta[] cartas;
    public int visionCartero;
    public int cantidadCartasActual;
    public boolean enPortal;
    
    public BusquedaPorAnchura busquedaPorAnchura;

    public Cartero(Laberinto laberinto) {
        
        this.enPortal=false;
        this.laberinto=laberinto;
        this.cantidadCartasActual=laberinto.cantidadCartas;
        this.cartas = new Carta[laberinto.cantidadCartas];
        this.visionCartero=RANGO_CELDA_VISION;
        this.cartero= new Celda(2,2,CARTERO);
        cartero.celdaAnterior = laberinto.celdas[cartero.x][cartero.y].tipo;
        laberinto.celdas[cartero.x][cartero.y].tipo=cartero.tipo;
        
        for (int i = 0; i < laberinto.cantidadCartas; i++) {
            //Posicion del cartero en laberinto*su tamaño + numero de carta * tamaño carta
            // - Numero de cartas-2 / 2 para que quede al centro
            cartas[i] = new Carta((cartero.x*SIZE_CELL+i*SIZE_CELL_M)
                    -((laberinto.cantidadCartas-2)/2)*SIZE_CELL_M,(cartero.y*SIZE_CELL)-(SIZE_CELL/2));
        }
        
        //recorro las cartas y le añado un destino a cada una
        for (int i = 0; i < laberinto.cantidadCartas; i++) {
            
            //validar
            System.out.println("Ingrese destino(portal) para carta "+i+":");
            int destino;
            Scanner entradaEscaner = new Scanner (System.in); //Creación de un objeto Scanner
            destino = entradaEscaner.nextInt(); //Invocamos un método sobre un objeto Scanner
            System.out.println ("El destino es el portal : \"" + destino +"\"");
            
            Celda celdaDestino = laberinto.portales.get(destino);
            Estado estadoDestino = new Estado((celdaDestino.x)/SIZE_CELL,(celdaDestino.y)/SIZE_CELL,'N',null);
            
            cartas[i].añadirDestino(estadoDestino);

        }
        
        busquedaPorAnchura=new BusquedaPorAnchura(laberinto,this);
        
        for (int i=0;i<laberinto.cantidadCartas;i++){
            this.busquedaPorAnchura.destinos.add(cartas[i].destino);
        }
        
        /*
        //Voy a recorrer las cartas, y le añado destinos a cada una de ellas
        
        for (int i = 0; i < NUMBER_CELL_WIDTH; i++) {
            for (int j = 0; j < NUMBER_CELL_HEIGHT; j++) {
                if(laberinto.celdas[i][j].tipo==PORTAL){
                     this.busquedaPorAnchura.destinos.add(new Estado(i,j,'N',null));
                }
            }
        }
        */
        
    }
    
    public void moverCelda(KeyEvent evento) {
        switch (evento.getKeyCode()) {
            case KeyEvent.VK_UP:
                moverCeldaArriba();
                break;
            case KeyEvent.VK_DOWN:
                moverCeldaAbajo();
                break;
            case KeyEvent.VK_LEFT:
                moverCeldaIzquierda();
                
                break;
            case KeyEvent.VK_RIGHT:
                moverCeldaDerecha();
                break;
        }
    }
    
    public void moverCeldaArriba() {
        if (cartero.y > 0) { //si se puede mover
            if(!hayObstaculo(ARRIBA)){
                avanzar(ARRIBA);
            }
        }
    }
    
    public void moverCeldaAbajo() {
        if (cartero.y < NUMBER_CELL_HEIGHT - 1) { //si se puede mover
            if(!hayObstaculo(ABAJO)){
                avanzar(ABAJO);
            }
        }
    }
    
    public void moverCeldaIzquierda() {
        if (cartero.x > 0) { //si se puede mover
            if(!hayObstaculo(IZQUIERDA)){
                avanzar(IZQUIERDA);
            }
        }
    }
    
    public void moverCeldaDerecha() {
        if (cartero.x < NUMBER_CELL_WIDTH - 1) { //si se puede mover
            if(!hayObstaculo(DERECHA)){
                avanzar(DERECHA);
            }
        }
    }   
    
    // Funcion que comprueba si existe obstaculo en la direccion indicada, o no
    public boolean hayObstaculo(char dir){
        
        //Variables para manejar la vision del caretro
        int i=0;
        int j=0;
        
        switch(dir){
            case ARRIBA:
                
                // Comprobacion de obstaculos en la direccion indicada
                if(laberinto.celdas[cartero.x][cartero.y - 1].tipo == PARED
                        || laberinto.celdas[cartero.x][cartero.y - 1].tipo == PEATON
                        || laberinto.celdas[cartero.x][cartero.y - 1].tipo == AUTOMOVIL
                        || laberinto.celdas[cartero.x][cartero.y - 1].tipo == PORTAL_CERRADO
                        || laberinto.celdas[cartero.x][cartero.y - 1].tipo == VACIA)
                    return true;
                
                //Comprobacion de obstaculos en la vision del cartero
                //Comprobar que la vision del cartero no se sale de los margenes del laberinto
                if(cartero.x-visionCartero>=0 && cartero.x+visionCartero<=NUMBER_CELL_WIDTH-1){
                    i=cartero.x-visionCartero;
                    j=cartero.x+visionCartero;
                } else {
                    if(cartero.x-visionCartero<0){
                        i=0;
                        j=cartero.x+visionCartero;
                    }
                    
                    if(cartero.x+visionCartero>NUMBER_CELL_WIDTH-1){
                        i=cartero.x-visionCartero;
                        j=NUMBER_CELL_WIDTH-1;
                    }
                }
                
                //While que recorre las celdas de vision del cartero
                while(i<j){
                    //Si hay un auto dentro del rango de vision
                    if(laberinto.celdas[i][cartero.y - 1].tipo==AUTOMOVIL){
                        //Si ese auto esta a la izquierda
                        if(i<cartero.x){
                            //Y ese auto viene a la derecha
                            if(laberinto.celdas[i][cartero.y - 1].direccion==DERECHA){
                                //Hay obstaculo
                                return true;
                            }
                        } else{
                            //Si está a la derecha y viene hacia la izquierda
                            if(laberinto.celdas[i][cartero.y - 1].direccion==IZQUIERDA){
                                //Hay Obstaculo
                                return true;
                            }
                        }
                    }
                    i++;
                }
                break;
                
            case ABAJO:
                if(laberinto.celdas[cartero.x][cartero.y + 1].tipo == PARED
                        || laberinto.celdas[cartero.x][cartero.y + 1].tipo == PEATON
                        || laberinto.celdas[cartero.x][cartero.y + 1].tipo == AUTOMOVIL
                        || laberinto.celdas[cartero.x][cartero.y + 1].tipo == PORTAL_CERRADO
                        || laberinto.celdas[cartero.x][cartero.y + 1].tipo == VACIA)
                    return true;
                
                //Comprobacion de obstaculos en la vision del cartero
                
                //Comprobar que la vision del cartero se encuentra dentro de los rangos de celdas
                if(cartero.x-visionCartero>=0 && cartero.x+visionCartero<=NUMBER_CELL_WIDTH-1){
                    i=cartero.x-visionCartero;
                    j=cartero.x+visionCartero;
                } else {
                    if(cartero.x-visionCartero<0){
                        i=0;
                        j=cartero.x+visionCartero;
                    }
                    
                    if(cartero.x+visionCartero>NUMBER_CELL_WIDTH-1){
                        i=cartero.x-visionCartero;
                        j=NUMBER_CELL_WIDTH-1;
                    }
                }
                
                //While que recorre las celdas de vision del cartero
                while(i<j){
                    //Si hay un auto dentro del rango de vision
                    if(laberinto.celdas[i][cartero.y + 1].tipo==AUTOMOVIL){
                        //Si ese auto esta a la izquierda
                        if(i<cartero.x){
                            //Y ese auto viene a la derecha
                            if(laberinto.celdas[i][cartero.y + 1].direccion==DERECHA){
                                //Hay obstaculo
                                return true;
                            }
                        } else{
                            //Si está a la derecha y viene hacia la izquierda
                            if(laberinto.celdas[i][cartero.y + 1].direccion==IZQUIERDA){
                                //Hay Obstaculo
                                return true;
                            }
                        }
                    }
                    i++;
                }
                
                break;
            case IZQUIERDA:
                if(laberinto.celdas[cartero.x-1][cartero.y].tipo == PARED
                        || laberinto.celdas[cartero.x-1][cartero.y].tipo == PEATON
                        || laberinto.celdas[cartero.x-1][cartero.y].tipo == AUTOMOVIL
                        || laberinto.celdas[cartero.x-1][cartero.y].tipo == PORTAL_CERRADO
                        || laberinto.celdas[cartero.x-1][cartero.y].tipo == VACIA)
                    return true;
                
                //Comprobacion de obstaculos en la vision del cartero
                
                //Comprobar que la vision del cartero se encuentra dentro de los rangos de celdas
                if(cartero.y-visionCartero>=0 && cartero.y+visionCartero<=NUMBER_CELL_HEIGHT-1){
                    i=cartero.y-visionCartero;
                    j=cartero.y+visionCartero;
                } else {
                    if(cartero.y-visionCartero<0){
                        i=0;
                        j=cartero.y+visionCartero;
                    }
                    
                    if(cartero.y+visionCartero>NUMBER_CELL_HEIGHT-1){
                        i=cartero.y-visionCartero;
                        j=NUMBER_CELL_HEIGHT-1;
                    }
                }
                
                //While que recorre las celdas de vision del cartero
                while(i<j){
                    //Si hay un auto dentro del rango de vision
                    if(laberinto.celdas[cartero.x-1][i].tipo==AUTOMOVIL){
                        //Si ese auto esta a la izquierda
                        if(i<cartero.y){
                            //Y ese auto viene a la derecha
                            if(laberinto.celdas[cartero.x-1][i].direccion==ABAJO){
                                //Hay obstaculo
                                return true;
                            }
                        } else{
                            //Si está a la derecha y viene hacia la izquierda
                            if(laberinto.celdas[cartero.x-1][i].direccion==ARRIBA){
                                //Hay Obstaculo
                                return true;
                            }
                        }
                    }
                    i++;
                }
                
                break;
            case DERECHA:
                if(laberinto.celdas[cartero.x+1][cartero.y].tipo == PARED
                        || laberinto.celdas[cartero.x+1][cartero.y].tipo == PEATON
                        || laberinto.celdas[cartero.x+1][cartero.y].tipo == AUTOMOVIL
                        || laberinto.celdas[cartero.x+1][cartero.y].tipo == PORTAL_CERRADO
                        || laberinto.celdas[cartero.x+1][cartero.y].tipo == VACIA)
                    return true;
                
                //Comprobacion de obstaculos en la vision del cartero
                
                //Comprobar que la vision del cartero se encuentra dentro de los rangos de celdas
                if(cartero.y-visionCartero>=0 && cartero.y+visionCartero<=NUMBER_CELL_HEIGHT-1){
                    i=cartero.y-visionCartero;
                    j=cartero.y+visionCartero;
                } else {
                    if(cartero.y-visionCartero<0){
                        i=0;
                        j=cartero.y+visionCartero;
                    }
                    
                    if(cartero.y+visionCartero>NUMBER_CELL_HEIGHT-1){
                        i=cartero.y-visionCartero;
                        j=NUMBER_CELL_HEIGHT-1;
                    }
                }
                
                //While que recorre las celdas de vision del cartero
                while(i<j){
                    //Si hay un auto dentro del rango de vision
                    if(laberinto.celdas[cartero.x+1][i].tipo==AUTOMOVIL){
                        //Si ese auto esta a la izquierda
                        if(i<cartero.y){
                            //Y ese auto viene a la derecha
                            if(laberinto.celdas[cartero.x+1][i].direccion==ABAJO){
                                //Hay obstaculo
                                return true;
                            }
                        } else{
                            //Si está a la derecha y viene hacia la izquierda
                            if(laberinto.celdas[cartero.x+1][i].direccion==ARRIBA){
                                //Hay Obstaculo
                                return true;
                            }
                        }
                    }
                    i++;
                }
                
                break;
        }
        return false;
    }
    
    // Funcion para avanzar a la diraccion indicada
    public void avanzar(char dir){
        
        switch(dir){
            case ARRIBA:
                
                // Asigna el tipo de celda de la posicion actual = celdaAnterior
                laberinto.celdas[cartero.x][cartero.y].tipo = cartero.celdaAnterior;
                
                // Almacena el tipo de la celda a la que se tiene que mover
                cartero.celdaAnterior = laberinto.celdas[cartero.x][cartero.y-1].tipo;
                
                // Si la celda a la que se esta moviendo es PORTAL
                if(cartero.celdaAnterior == PORTAL){
                    
                    //Muestra una alerta sin interrumpir el flujo de trabajo( de otra forma detenia los otros threads
                    // me preocupa que genere demasiados hilos
                    Thread t = new Thread(new Runnable(){
                           public void run(){
                               
                               showMessageDialog(null, "Cartero en Portal");
                           }
                       });
                     t.start();
                     
                    //Quitar carta
                    cantidadCartasActual--;
                    enPortal=true;
                } else {
                    enPortal=false;
                }
                
                // Se "mueve" logicamente el cartero
                cartero.y = cartero.y - 1;
                laberinto.celdas[cartero.x][cartero.y].tipo = CARTERO;
                laberinto.celdas[cartero.x][cartero.y].celdaAnterior = cartero.celdaAnterior;
                
                // Las cartas se mueven junto al cartero
                for(int i=0;i<laberinto.cantidadCartas;i++){
                    cartas[i].y=(cartas[i].y-SIZE_CELL);
                }
                
                break;
            case ABAJO:
                laberinto.celdas[cartero.x][cartero.y].tipo = cartero.celdaAnterior;
                cartero.celdaAnterior = laberinto.celdas[cartero.x][cartero.y+1].tipo;
                
                if(cartero.celdaAnterior == PORTAL){
                   
                    //Muestra una alerta sin interrumpir el flujo de trabajo( de otra forma detenia los otros threads
                    // me preocupa que genere demasiados hilos
                    Thread t = new Thread(new Runnable(){
                           public void run(){
                               showMessageDialog(null, "Cartero en Portal");
                           }
                       });
                     t.start();
                     
                    cantidadCartasActual--;
                    enPortal=true;
                } else {
                    enPortal=false;
                }
                cartero.y = cartero.y + 1;
                laberinto.celdas[cartero.x][cartero.y].tipo = CARTERO;
                laberinto.celdas[cartero.x][cartero.y].celdaAnterior = cartero.celdaAnterior;
                
                for(int i=0;i<laberinto.cantidadCartas;i++){
                    cartas[i].y=(cartas[i].y+SIZE_CELL);
                }
                break;
            case IZQUIERDA:
                laberinto.celdas[cartero.x][cartero.y].tipo = cartero.celdaAnterior;
                
                cartero.celdaAnterior = laberinto.celdas[cartero.x-1][cartero.y].tipo;
                if(cartero.celdaAnterior == PORTAL){
                   
                    //Muestra una alerta sin interrumpir el flujo de trabajo( de otra forma detenia los otros threads
                    // me preocupa que genere demasiados hilos
                    Thread t = new Thread(new Runnable(){
                           public void run(){
                               showMessageDialog(null, "Cartero en Portal");
                           }
                       });
                     t.start();
                     
                    cantidadCartasActual--;
                    enPortal=true;
                } else {
                    enPortal=false;
                }
                cartero.x = cartero.x - 1;
                laberinto.celdas[cartero.x][cartero.y].tipo = CARTERO;
                laberinto.celdas[cartero.x][cartero.y].celdaAnterior = cartero.celdaAnterior;
                
                for(int i=0;i<laberinto.cantidadCartas;i++){
                    cartas[i].x=(cartas[i].x-SIZE_CELL);
                }
                break;
            case DERECHA:
                laberinto.celdas[cartero.x][cartero.y].tipo = cartero.celdaAnterior;
                
                
                cartero.celdaAnterior = laberinto.celdas[cartero.x+1][cartero.y].tipo;
                if(cartero.celdaAnterior == PORTAL){
                   
                    //Muestra una alerta sin interrumpir el flujo de trabajo( de otra forma detenia los otros threads
                    // me preocupa que genere demasiados hilos
                    Thread t = new Thread(new Runnable(){
                           public void run(){
                               showMessageDialog(null, "Cartero en Portal");
                           }
                       });
                     t.start();
                    
                    cantidadCartasActual--;
                    enPortal=true;
                } else {
                    enPortal=false;
                }
                cartero.x = cartero.x + 1;
                laberinto.celdas[cartero.x][cartero.y].tipo = CARTERO;                
                laberinto.celdas[cartero.x][cartero.y].celdaAnterior = cartero.celdaAnterior;

                for(int i=0;i<laberinto.cantidadCartas;i++){
                    cartas[i].x=(cartas[i].x+SIZE_CELL);
                }
                break;
                
        }
        
    }
    
    @Override
    public void run() {
        laberinto.lienzoPadre.repaint();
    }
    
}