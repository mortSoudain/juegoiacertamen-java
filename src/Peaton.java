import java.util.TimerTask;

public class Peaton extends TimerTask implements Constantes{
    
    public Laberinto laberinto;
    //Celda creada solamente para manejar con mayor facilidad los indices del laberinto
    public Celda peaton;

    
    public Peaton(Laberinto laberinto) {
        this.laberinto=laberinto;
        
        // Validacion para crear los peatones solo en las aceras
        // Los peatones se generan en posiciones aleatorias
        boolean creado=false;
        do{
            int x = numeroAleatorio(0,NUMBER_CELL_WIDTH-1);
            int y = numeroAleatorio(0,NUMBER_CELL_HEIGHT-1);
            
            if(laberinto.celdas[x][y].tipo==ACERA){
                peaton=new Celda(x,y,PEATON);
                creado=true;
            }
            
        } while(!creado);

        
        peaton.celdaAnterior = laberinto.celdas[peaton.x][peaton.y].tipo;
        laberinto.celdas[peaton.x][peaton.y].tipo=PEATON;
        laberinto.celdas[peaton.x][peaton.y].direccion=direccionAleatoria();
    }
    
    public void moverPeaton(){
        switch(laberinto.celdas[peaton.x][peaton.y].direccion){
            case IZQUIERDA:
                moverPeatonIzquierda();
                break;
            case ARRIBA:
                moverPeatonArriba();
                break;
            case DERECHA:
                moverPeatonDerecha();
                break;
            case ABAJO:
                moverPeatonAbajo();
                break;
        }
    }
    
    private void moverPeatonArriba() {
        if (peaton.y > 0) { // Si el peaton no ha llegado al limite superior
            if(!hayObstaculo(ARRIBA)){ //Si no hay obstaculo, avanzar
                avanzar(ARRIBA);
            } else { //Si hay obstaculo, cambiar direccion
                laberinto.celdas[peaton.x][peaton.y].direccion=DERECHA;
            }
        } else { //Auto en el limite superior
            laberinto.celdas[peaton.x][peaton.y].tipo=peaton.celdaAnterior;
            //peaton.x=numeroAleatorio(0,NUMBER_CELL_WIDTH-1);
            peaton.y=NUMBER_CELL_HEIGHT-1;
            laberinto.celdas[peaton.x][peaton.y].tipo=PEATON;
            laberinto.celdas[peaton.x][peaton.y].direccion=ARRIBA;
        }
    }
    
    private void moverPeatonAbajo() {
        if (peaton.y < NUMBER_CELL_HEIGHT - 1) { //si se puede mover
            if(!hayObstaculo(ABAJO)){
                avanzar(ABAJO);
            } else {
                laberinto.celdas[peaton.x][peaton.y].direccion=IZQUIERDA;
            }
        } else{
            laberinto.celdas[peaton.x][peaton.y].tipo=peaton.celdaAnterior;
            //peaton.x=numeroAleatorio(0,NUMBER_CELL_WIDTH-1);
            peaton.y=0;
            laberinto.celdas[peaton.x][peaton.y].tipo=PEATON;
            laberinto.celdas[peaton.x][peaton.y].direccion=ABAJO;
        }
    }
    
    private void moverPeatonDerecha() {
        if (peaton.x < NUMBER_CELL_WIDTH - 1) { //si se puede mover
            if(!hayObstaculo(DERECHA)){
                avanzar(DERECHA);
            } else {
                laberinto.celdas[peaton.x][peaton.y].direccion=ABAJO;
            }
        } else {
            laberinto.celdas[peaton.x][peaton.y].tipo=peaton.celdaAnterior;
            peaton.x=0;
            //peaton.y=numeroAleatorio(0,NUMBER_CELL_HEIGHT-1);
            laberinto.celdas[peaton.x][peaton.y].tipo=PEATON;
            laberinto.celdas[peaton.x][peaton.y].direccion=DERECHA;
        }
    }
    
    private void moverPeatonIzquierda() {
        if (peaton.x > 0) { //si se puede mover
            if(!hayObstaculo(IZQUIERDA)){
                avanzar(IZQUIERDA);
            } else{
                laberinto.celdas[peaton.x][peaton.y].direccion=ARRIBA;
            }
        } else {
            laberinto.celdas[peaton.x][peaton.y].tipo=peaton.celdaAnterior;
            peaton.x=NUMBER_CELL_WIDTH-1;
            //peaton.y=numeroAleatorio(0,NUMBER_CELL_HEIGHT-1);
            laberinto.celdas[peaton.x][peaton.y].tipo=PEATON;
            laberinto.celdas[peaton.x][peaton.y].direccion=IZQUIERDA;
        }
    }
    
    public boolean hayObstaculo(char dir){
        switch(dir){
            case ARRIBA:
                if(laberinto.celdas[peaton.x][peaton.y - 1].tipo != ACERA &&
                        laberinto.celdas[peaton.x][peaton.y-1].tipo != PASOH &&
                        laberinto.celdas[peaton.x][peaton.y-1].tipo != PASOV)
                    return true;
                break;
            case ABAJO:
                if(laberinto.celdas[peaton.x][peaton.y + 1].tipo != ACERA &&
                        laberinto.celdas[peaton.x][peaton.y+1].tipo != PASOH &&
                        laberinto.celdas[peaton.x][peaton.y+1].tipo != PASOV)
                    return true;
                
                break;
            case IZQUIERDA:
                if(laberinto.celdas[peaton.x-1][peaton.y].tipo != ACERA &&
                        laberinto.celdas[peaton.x-1][peaton.y].tipo != PASOH &&
                        laberinto.celdas[peaton.x-1][peaton.y].tipo != PASOV){
                    return true;
                }
                break;
            case DERECHA:
                if(laberinto.celdas[peaton.x+1][peaton.y].tipo != ACERA &&
                        laberinto.celdas[peaton.x+1][peaton.y].tipo != PASOH &&
                        laberinto.celdas[peaton.x+1][peaton.y].tipo != PASOV)
                    return true;
                break;
        }
        return false;
    }
    
    public void avanzar(char dir){
        switch(dir){
            case ARRIBA:
                laberinto.celdas[peaton.x][peaton.y].tipo = peaton.celdaAnterior;
                laberinto.celdas[peaton.x][peaton.y].direccion = NULA;
                peaton.celdaAnterior = laberinto.celdas[peaton.x][peaton.y-1].tipo;
                peaton.y = peaton.y - 1;
                laberinto.celdas[peaton.x][peaton.y].celdaAnterior=peaton.celdaAnterior;
                laberinto.celdas[peaton.x][peaton.y].color=laberinto.celdas[peaton.x][peaton.y+1].color;
                laberinto.celdas[peaton.x][peaton.y].tipo = PEATON;
                laberinto.celdas[peaton.x][peaton.y].direccion = ARRIBA;                
                break;
            case 'D':
                laberinto.celdas[peaton.x][peaton.y].tipo = peaton.celdaAnterior;
                laberinto.celdas[peaton.x][peaton.y].direccion = NULA;
                peaton.celdaAnterior = laberinto.celdas[peaton.x][peaton.y+1].tipo;
                peaton.y = peaton.y + 1;
                laberinto.celdas[peaton.x][peaton.y].celdaAnterior=peaton.celdaAnterior;
                laberinto.celdas[peaton.x][peaton.y].color=laberinto.celdas[peaton.x][peaton.y-1].color;
                laberinto.celdas[peaton.x][peaton.y].tipo = PEATON;
                laberinto.celdas[peaton.x][peaton.y].direccion = ABAJO;                
                break;
            case 'L':
                laberinto.celdas[peaton.x][peaton.y].tipo = peaton.celdaAnterior;
                laberinto.celdas[peaton.x][peaton.y].direccion = NULA;
                peaton.celdaAnterior = laberinto.celdas[peaton.x-1][peaton.y].tipo;
                peaton.x = peaton.x - 1;
                laberinto.celdas[peaton.x][peaton.y].celdaAnterior=peaton.celdaAnterior;
                laberinto.celdas[peaton.x][peaton.y].color=laberinto.celdas[peaton.x+1][peaton.y].color;
                laberinto.celdas[peaton.x][peaton.y].tipo = PEATON;
                laberinto.celdas[peaton.x][peaton.y].direccion = IZQUIERDA;                
                break;
            case 'R':
                laberinto.celdas[peaton.x][peaton.y].tipo = peaton.celdaAnterior;
                laberinto.celdas[peaton.x][peaton.y].direccion = NULA;
                peaton.celdaAnterior = laberinto.celdas[peaton.x+1][peaton.y].tipo;
                peaton.x = peaton.x + 1;    
                laberinto.celdas[peaton.x][peaton.y].celdaAnterior=peaton.celdaAnterior;
                laberinto.celdas[peaton.x][peaton.y].color=laberinto.celdas[peaton.x-1][peaton.y].color;
                laberinto.celdas[peaton.x][peaton.y].tipo = PEATON;
                laberinto.celdas[peaton.x][peaton.y].direccion = DERECHA;                
                break;
        }
    }
    
    @Override
    public void run() {
        moverPeaton();
        laberinto.lienzoPadre.repaint();
    }
    
}