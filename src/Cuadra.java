
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 3ms
 */
public class Cuadra implements Constantes{
    
    public int x;
    public int y;
    
    public int cantidadPeatones;
    public char celdas[][];
    public Laberinto laberinto;
    
    public Cuadra(int x, int y){
        this.x=x;
        this.y=y;
        celdas = new char[NUMBER_CELL_CUADRA][NUMBER_CELL_CUADRA];
        this.cantidadPeatones=numeroAleatorio(1,100);
        leerCuadraDesdeTxt("cuadra");
        //imprimirCuadra();
        
    }
    
    public void leerCuadraDesdeTxt(String txt){
            // Funcion que lee la información del archivo de texto y la pasa a las celdas
        
        File archivo = null;
        FileReader fileread = null;
        BufferedReader bufferread = null;
        int contador =0;
        
        try {
            // Apertura del fichero y creacion de BufferedReader para
            // leer linea de archivo(disponer del metodo readLine()).
            archivo = new File ("recursos/mapas/"+txt+".txt");
            fileread = new FileReader (archivo);
            bufferread = new BufferedReader(fileread);
            
            // Lectura del fichero
            String linea;
            while((linea=bufferread.readLine()) != null){
                for(int i=0;i<NUMBER_CELL_CUADRA;i++){
                    celdas[i][contador] = linea.charAt(i);
                }
                contador++;
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }finally{
            // En el finally cerramos el fichero
            try{
                if( null != fileread ){
                    fileread.close();
                }
            }catch (Exception e2){
                e2.printStackTrace();
            }
        }
        
    }
    
    public void imprimirCuadra(){
        for(int i=0; i<NUMBER_CELL_CUADRA; i++){
            for(int j=0;j<NUMBER_CELL_CUADRA;j++){
                System.out.print(celdas[i][j]+" ");
            }
            System.out.println();
        }
    }
    
    public void insertarCuadraEnLaberinto(Laberinto laberinto){
        this.laberinto=laberinto;
        
        for(int i=0; i<NUMBER_CELL_CUADRA; i++){
            for(int j=0;j<NUMBER_CELL_CUADRA;j++){
                if(x+i<NUMBER_CELL_WIDTH && y+j<NUMBER_CELL_HEIGHT)
                    laberinto.celdas[x+i][y+j].cantidadPeatones=this.cantidadPeatones;
                    laberinto.celdas[x+i][y+j].tipo=celdas[i][j];
                    
            }
        }
    }
    
    public void insertarPortalEnCuadra(){
        
        boolean insertado=false;
        do{
            int x=numeroAleatorio(2,NUMBER_CELL_CUADRA-2);
            int y=numeroAleatorio(2,NUMBER_CELL_CUADRA-2);
        
            if(celdas[x][y]==PARED){
                celdas[x][y]=PORTAL;
                insertado=true;
            }
            

        } while(!insertado);
        
    }

    
}